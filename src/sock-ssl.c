/* $Id$
 *
 * Handles the ugliness of using the openSSL library.
 *
 * Copyright (C) 2000 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * Authors: Robey Pointer <robey@eazel.com>
 *          Mike Fleming <mfleming@eazel.com>
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/* only build this file if openssl is installed */
#ifdef HAVE_OPENSSL

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <glib.h>

#include <sys/types.h>
#include <dirent.h>

#include "log.h"
#include "sock-ssl.h"

/* global SSL context */
static SSL_CTX *ssl_context = NULL;
static gboolean gl_ignore_server_cert = FALSE;


static int
verify_callback(int state, X509_STORE_CTX *x509_context)
{
	char name[256];
	int err;
	int ret;

	X509_NAME_oneline (X509_get_subject_name (x509_context->current_cert), name, sizeof(name)-1);

	name [sizeof (name)-1] = '\0';

	/* By default, we don't get involved with the verification process */
	ret = state;
	
	if (gl_ignore_server_cert) {
		log ("WARNING: ignore-server-cert set, treating cert as verified: '%s'", name);
		ret = 1;
	} else {
		if (state == 0) {
			err = X509_STORE_CTX_get_error (x509_context);
			log ("ssl cert error: %s: %s", X509_verify_cert_error_string(x509_context->error), name);
		}
	}

	return ret;
}

/* stolen from openssl */
static void
ssl_log_error(const char *prefix)
{
	unsigned long l;
	char buf[200];
	const char *file, *data;
	int line, flags;

	l = ERR_get_error_line_data (&file, &line, &data, &flags);
	if (l) {
		log ("%s%s: %s", prefix, ERR_error_string (l, buf),
		     (flags & ERR_TXT_STRING) ? data : "");
	} else {
		log ("%s(no ssl error)", prefix);
	}
}

static gboolean
string_ends_in (const char *string, const char *suffix)
{
	size_t string_len, suffix_len;

	string_len = strlen (string);
	suffix_len = strlen (suffix);

	if (suffix_len > string_len) {
		return FALSE;
	} else {
		return 0 == strcmp (string + strlen (string) - strlen (suffix), suffix);
	}
}

static void
load_all_certs_in_directory (const char *cert_directory)
{
	DIR *dir_handle;
	struct dirent *entry;
	char *full_filename;

	if (NULL == cert_directory) {
		return;
	}


	dir_handle = opendir (cert_directory);

	if (NULL == dir_handle) {
		return;
	}

	log ("Loading all certs in %s", cert_directory);

	while (NULL != (entry = readdir (dir_handle))) {
		if (string_ends_in (entry->d_name, ".pem")) {
			full_filename = g_strconcat (cert_directory, "/", entry->d_name, NULL);
			if (SSL_CTX_load_verify_locations (ssl_context, full_filename, NULL)) {
				log ("PEM certificates in '%s' loaded", entry->d_name);
			} else {
				log ("ERROR loading PEM certificates in '%s'", entry->d_name);
			}
			g_free (full_filename);
			full_filename = NULL;
		}
	}
}

/* initialize the openssl library and set up some callbacks and paths.
 *     cert_directory: a directory where CA certs can be found
 *     cert_file: a single CA cert file
 * either cert_directory or cert_file may be NULL.  if both are NULL, then
 * no cert verification will succeed, so all connections will fail if we're
 * checking the server cert.  in this case, you can call ssl_set_ignore_server_certs.
 * Of course, this significantly hobbles security.
 *
 * generally you will just pass in the path to the cert file that contains
 * the public key for the CA that signed the upstream web server's key.
 * for example, for the apache-modssl demo key set, you would use the
 * snakeoil-ca-rsa.crt file.
 */
void
eazel_init_ssl(char *cert_directory, char *cert_file)
{
	/* this is all MAGIC.  nobody knows how it works and openSSL is undocumented.
	 * touch this at your own peril!! 
	 */
	SSLeay_add_ssl_algorithms ();
	SSL_load_error_strings ();

	/* apparently this can never error */
	ssl_context = SSL_CTX_new (SSLv3_client_method ());
	if (ssl_context == NULL) {
		ssl_log_error ("FATAL: openssl context creation failed: ");
		exit (1);
	}

	/* set verify level on certs */
	if (!SSL_CTX_set_default_verify_paths (ssl_context)) {
		ssl_log_error ("FATAL: openssl default configuration failed: ");
		exit (1);
	}

	if (NULL != cert_file) {
		if (!SSL_CTX_load_verify_locations (ssl_context, cert_file, NULL)) {
			ssl_log_error ("FATAL: openssl cert loading failed: ");
			exit(1);
		}
	}

	if (NULL != cert_directory) {
		load_all_certs_in_directory (cert_directory);
	}

	SSL_CTX_set_verify (ssl_context, SSL_VERIFY_PEER, verify_callback);
}

gboolean
ssl_set_preferred_cipher_list (const char *preferred_cipher_list)
{
	gboolean ret;

	g_return_val_if_fail (ssl_context != NULL, FALSE); 

	if (NULL != preferred_cipher_list) {
		ret = (0 != SSL_CTX_set_cipher_list (ssl_context, preferred_cipher_list));

		if (ret) {
			log ("SSL cipher list set to '%s'", preferred_cipher_list);
		}
	} else {
		ret = FALSE;
	}

	return ret;
}

void
ssl_set_ignore_server_cert (gboolean ignore_server_cert)
{
	gl_ignore_server_cert = ignore_server_cert;

	if (gl_ignore_server_cert) {
		log ("WARNING: ignore-server-cert set to TRUE, all SSL certs will be accepted");
	}
}

/* given a normal socket that's just been connected somewhere, attempt to open an SSL
 * session.
 * on success, returns an SSL handle which can be used to do SSL_read/SSL_write.
 */
SSL *
ssl_begin_ssl(int fd, int *error)
{
	SSL *ssl_handle;
	int err;
	int bits, alg_bits;
	X509 *server_cert;

	*error = 0;

	/* AFAIK, these 3 calls should never fail.  -robey */

	ssl_handle = SSL_new (ssl_context);
	if (! ssl_handle) {
		ssl_log_error ("error: SSL_new: ");
		*error = SOCKET_SSL_ERROR_OPENSSL;
		goto out;
	}

	err = SSL_set_fd (ssl_handle, fd);
	if (err <= 0) {
		ssl_log_error ("error: SSL_set_fd: ");
		*error = SOCKET_SSL_ERROR_OPENSSL;
		goto bad;
	}

	err = SSL_connect (ssl_handle);
	if (err <= 0) {
		ssl_log_error ("error: SSL_connect: ");
		*error = SOCKET_SSL_ERROR_OPENSSL;
		goto bad;
	}

	bits = SSL_get_cipher_bits (ssl_handle, &alg_bits);
	if (bits <= 0) {
		log ("error: SSL not supported by server");
		*error = SOCKET_SSL_ERROR_NOT_SUPPORTED;
		goto bad;
	}

	log ("SSL connect: cipher %s, %d bits", SSL_get_cipher_name (ssl_handle), bits);

	server_cert = SSL_get_peer_certificate (ssl_handle);
	if (server_cert == NULL) {
		ssl_log_error ("error: no server cert: ");
		*error = SOCKET_SSL_ERROR_NO_CERT;
		goto bad;
	}

	X509_free (server_cert);
	server_cert = NULL;

out:
	return ssl_handle;

bad:
	SSL_free(ssl_handle);
	return NULL;
}


#endif	/* HAVE_OPENSSL */
