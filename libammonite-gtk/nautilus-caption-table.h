/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* nautilus-caption-table.h - An easy way to do tables of aligned captions.

   Copyright (C) 1999, 2000 Eazel, Inc.

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Authors: Ramiro Estrugo <ramiro@eazel.com>
*/

#ifndef AMMONITE_CAPTION_TABLE_H
#define AMMONITE_CAPTION_TABLE_H

#include <gtk/gtktable.h>
#include <gnome.h>

/*
 * AmmoniteCaptionTable is a GtkTable sublass that allows you to painlessly
 * create tables of nicely aligned captions.
 */

BEGIN_GNOME_DECLS

#define NAUTILUS_TYPE_CAPTION_TABLE			(ammonite_caption_table_get_type ())
#define AMMONITE_CAPTION_TABLE(obj)		(GTK_CHECK_CAST ((obj), NAUTILUS_TYPE_CAPTION_TABLE, AmmoniteCaptionTable))
#define AMMONITE_CAPTION_TABLE_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), NAUTILUS_TYPE_CAPTION_TABLE, AmmoniteCaptionTableClass))
#define NAUTILUS_IS_CAPTION_TABLE(obj)		(GTK_CHECK_TYPE ((obj), NAUTILUS_TYPE_CAPTION_TABLE))
#define NAUTILUS_IS_CAPTION_TABLE_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), NAUTILUS_TYPE_CAPTION_TABLE))


typedef struct _AmmoniteCaptionTable		AmmoniteCaptionTable;
typedef struct _AmmoniteCaptionTableClass	AmmoniteCaptionTableClass;
typedef struct _AmmoniteCaptionTableDetail	AmmoniteCaptionTableDetail;

struct _AmmoniteCaptionTable
{
	GtkTable table;

	AmmoniteCaptionTableDetail *detail;
};

struct _AmmoniteCaptionTableClass
{
	GtkTableClass parent_class;

	void (*activate) (GtkWidget *caption_table, int active_entry);
	void (*tab_out) (GtkWidget *caption_table);
};

GtkType    ammonite_caption_table_get_type           (void);
GtkWidget* ammonite_caption_table_new                (guint                 num_rows);
void       ammonite_caption_table_set_row_info       (AmmoniteCaptionTable *caption_table,
						      guint                 row,
						      const char           *label_text,
						      const char           *entry_text,
						      gboolean              entry_visibility,
						      gboolean              entry_readonly);
void       ammonite_caption_table_set_entry_text     (AmmoniteCaptionTable *caption_table,
						      guint                 row,
						      const char           *entry_text);
void       ammonite_caption_table_set_entry_readonly (AmmoniteCaptionTable *caption_table,
						      guint                 row,
						      gboolean              readonly);
void       ammonite_caption_table_entry_grab_focus   (AmmoniteCaptionTable *caption_table,
						      guint                 row);
char*      ammonite_caption_table_get_entry_text     (AmmoniteCaptionTable *caption_table,
						      guint                 row);
guint      ammonite_caption_table_get_num_rows       (AmmoniteCaptionTable *caption_table);
void       ammonite_caption_table_resize             (AmmoniteCaptionTable *caption_table,
						      guint                 num_rows);

BEGIN_GNOME_DECLS

#endif /* AMMONITE_CAPTION_TABLE_H */


