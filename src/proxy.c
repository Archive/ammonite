/* $Id$
 *
 * A trivial proxy server for relaying Eazel HTTP connections.  Sets up
 * initial config options, daemonizes if necessary, and then stays in a
 * socket event loop until death.
 *
 * Copyright (C) 1998  Steven Young
 * Copyright (C) 1999  Robert James Kaes (rjkaes@flarenet.com)
 * Copyright (C) 2000  Chris Lightfoot (chris@ex-parrot.com)
 * Copyright (C) 2000  Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * Authors: Steven Young
 *          Robert James Kaes <rjkaes@flarenet.com>
 *          Chris Lightfoot <chris@ex-parrot.com>
 *          Robey Pointer <robey@eazel.com>
 *          Mike Fleming <mfleming@eazel.com>
 */

#undef CHECK_FOR_LEAKS

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sysexits.h>
#include <popt.h>
#include <glib.h>

#ifndef SIMPLE_PROXY
#include <liboaf/liboaf.h>
#endif /* SIMPLE_PROXY */

#ifdef CHECK_FOR_LEAKS
#include <dlfcn.h>
#endif

#include "sock.h"
#include "proxy.h"
#include "log.h"
#include "utils.h"
#ifndef SIMPLE_PROXY
#include "impl-eazelproxy.h"
#endif /* SIMPLE_PROXY */
#include "sock-ssl.h"
#include "request.h"
#include "digest.h"

#ifndef SIMPLE_PROXY
#include "util-gconf.h"
#endif /* SIMPLE_PROXY */

#define PORT_HTTP_DEFAULT 80
#define PORT_HTTPS_DEFAULT 443
#define PORT_HTTP_PROXY_DEFAULT 8080

#define PATH_GCONF_GNOME_VFS "/system/gnome-vfs/"
#define ITEM_GCONF_HTTP_PROXY_PORT "http-proxy-port"
#define ITEM_GCONF_HTTP_PROXY_HOST "http-proxy-host"
#define KEY_GCONF_HTTP_PROXY_PORT (PATH_GCONF_GNOME_VFS ITEM_GCONF_HTTP_PROXY_PORT)
#define KEY_GCONF_HTTP_PROXY_HOST (PATH_GCONF_GNOME_VFS ITEM_GCONF_HTTP_PROXY_HOST)

#define ITEM_GCONF_USE_HTTP_PROXY "use-http-proxy"
#define KEY_GCONF_USE_HTTP_PROXY (PATH_GCONF_GNOME_VFS ITEM_GCONF_USE_HTTP_PROXY)

#define KEY_GCONF_DEFAULT_SSL_CERT "/apps/eazel-trilobite/ssl-cert"
#define KEY_GCONF_DEFAULT_SSL_CERT_DIR "/apps/eazel-trilobite/ssl-cert-dir"
#define KEY_GCONF_DISABLE_SSL "/apps/eazel-trilobite/ssl-disable"
#define KEY_GCONF_SSL_CIPHER "/apps/eazel-trilobite/ssl-prefer-cipher"
#define KEY_GCONF_SSL_IGNORE_SERVER_CERT "/apps/eazel-trilobite/ssl-ignore-server-cert"

#define EAZEL_DEFAULT_CERT_DIR "/nautilus/certs"

void takesig (int sig);


/* global config */
Config config = {
	NULL,		/* logfile handle */
	NULL,		/* logfile name */
        NULL,		/* target path */
	NULL,		/* upstream proxy host */
	PORT_HTTP_DEFAULT,/* upstream proxy port */
	FALSE, 		/* was -u specified? */
	"127.0.0.1",	/* local host (normally bind only to loopback) */
	0,		/* local port -- for debug only -- if 0, search */
	FALSE,		/* append to logfile (instead of truncate)? */
	FALSE,		/* use stderr for logging */
	FALSE,		/* use ssl for outbound connections (only when built against openssl) */
	TRUE,		/* use OAF */
	FALSE,		/* quit/terminate */
	TRUE,		/* check uid */
	NULL		/* mirror dir */
};

struct stat_s stats;

GMainLoop *l_main_loop = NULL;

char *gl_opt_certfile = NULL;
char *gl_opt_cert_dir = NULL;

int gl_opt_no_ssl = 0;
gboolean gl_gconf_ssl_disabled = FALSE;
char *gl_gconf_ssl_cert = NULL;
char *gl_gconf_ssl_cert_dir = NULL;
char *gl_opt_proxy_auth = NULL;

static const struct poptOption popt_options[] = {
	{"target-path", 't', POPT_ARG_STRING, NULL, 't', "force all web pages to a specific site", "url"},
	{"logfile", 'l', POPT_ARG_STRING, NULL, 'l', "log to a logfile", "filename"},
	{"append", 'L', POPT_ARG_NONE, NULL, 'L', "append to logfile instead of truncating", NULL},
#ifndef SIMPLE_PROXY
	{"no-oaf", 'n', POPT_ARG_NONE, NULL, 'n', "don't use OAF or CORBA hooks", NULL},
#ifdef HAVE_OPENSSL
	{"no-ssl", '\0', POPT_ARG_NONE, &gl_opt_no_ssl, 0, "Do not make outbound connections use SSL", NULL },
	{"ssl-cert", '\0', POPT_ARG_STRING, &gl_opt_certfile, 0, "SSL certificate to use to verify server ", "cert-file" },
	{"ssl-cert-dir", '\0', POPT_ARG_STRING, &gl_opt_cert_dir, 0, "Directory containing SSL certs, all of which will be used.", "cert-directory" },
#endif /* HAVE_OPENSSL */
#endif /* SIMPLE_PROXY */
	{"upstream-proxy", 'u', POPT_ARG_STRING, NULL, 'u', "use an upstream proxy for connections",
		"host[:port]"},
#ifndef NO_DEBUG_MIRRORING
	{"no-daemon", 'd', POPT_ARG_NONE, NULL, 'd', "do not daemonize", NULL},
	{"stderr", 'e', POPT_ARG_NONE, NULL, 'e', "use stderr for logging (debug)", NULL},
	{"no-check", 'r', POPT_ARG_NONE, NULL, 'r', "don't check the uid of connecting clients", NULL},
	{"bind-address", 'b', POPT_ARG_STRING | POPT_ARGFLAG_DOC_HIDDEN, NULL, 'b', "specify address to bind to (debug)", "address"},
	{"port", 'p', POPT_ARG_STRING | POPT_ARGFLAG_DOC_HIDDEN, NULL, 'p', "specify listen port number (debug)", "port"},
	{"mirror", 'm', POPT_ARG_STRING | POPT_ARGFLAG_DOC_HIDDEN, NULL, 'm', "log i/o, place files in directory (debug)", "log-directory"},
#endif /* DEBUG */
	{"version", 'v', POPT_ARG_NONE, NULL, 'v', "display version string", NULL},
#ifndef SIMPLE_PROXY
	{NULL, '\0', POPT_ARG_INCLUDE_TABLE, oaf_popt_options, 0, "OAF options", NULL},
#endif /* SIMPLE_PROXY */
	{"proxy-auth", 'a', POPT_ARG_STRING, &gl_opt_proxy_auth, 'a', "Enable basic proxy authentication", "user:password"},
	POPT_AUTOHELP
	{NULL, 'h', POPT_ARG_NONE | POPT_ARGFLAG_DOC_HIDDEN, NULL, 'h', "", NULL},
	{NULL, '\0', 0, NULL, 0, NULL}
};


#ifdef CHECK_FOR_LEAKS
static void
leak_checker_init (const char *path)
{
	void (*real_leak_checker_init) (const char *path);

	real_leak_checker_init = dlsym (RTLD_NEXT, "nautilus_leak_checker_init");
	if (real_leak_checker_init) {
		(*real_leak_checker_init) (path);
	}
}

static void
leak_checker_dump_internal (int stack_grouping_depth,
			    int stack_print_depth,
			    int max_count,
			    int sort_by_count)
{
	void (*real_leak_checker_dump) (int stack_grouping_depth,
					int stack_print_depth,
					int max_count,
					int sort_by_count);

	real_leak_checker_dump = dlsym (RTLD_NEXT, "nautilus_leak_print_leaks");
	if (real_leak_checker_dump) {
		(*real_leak_checker_dump) (stack_grouping_depth, stack_print_depth,
					   max_count, sort_by_count);
	}
}

static void
leak_checker_dump (void)
{
	leak_checker_dump_internal (8, 15, 100, TRUE);
}
#endif

/*
 * Handle a signal
 */
void 
takesig (int sig)
{
	switch (sig) {
	case SIGUSR1:
		socket_log_debug ();
		log ("::: stats: RX %d, TX %d", stats.num_rx, stats.num_tx);
		break;
	case SIGHUP:
		if (config.logf)
			ftruncate (fileno (config.logf), 0);
		log ("SIGHUP received, cleared log...");
		break;
	case SIGKILL:
	case SIGTERM:
		g_main_quit (l_main_loop);
		break;
	case SIGALRM:
		/* ignore */
		break;
	}
	if (sig != SIGTERM)
		signal (sig, takesig);
	signal (SIGPIPE, SIG_IGN);
}


static void display_version (void)
{
	printf("\n" PACKAGE " version " VERSION "\n");
#ifdef HAVE_OPENSSL
	fprintf (stderr, "\tincluding openSSL support\n");
#endif
	printf("\n");

	printf("Copyright 1998       Steven Young (sdyoung@well.com)\n");
	printf("Copyright 1998-1999  Robert James Kaes (rjkaes@flarenet.com)\n");
	printf("Copyright 2000       Chris Lightfoot (chris@ex-parrot.com)\n");
	printf("Copyright 2000       Eazel, Inc.\n");

	printf("This software is licensed under the GNU General Public License (GPL).\n");
	printf("See the file 'COPYING' included with " PACKAGE " source.\n\n");
}

#ifndef SIMPLE_PROXY
/* This function is called when either use-http-proxy or http-proxy change */
static void /*UtilGConfCb*/
watch_gconf_proxy_cb (const UtilGConfWatchVariable *watched, 
		      const GConfValue *new_value) 
{
	GConfValue *val_use = NULL;
	GConfValue *val_proxy_host = NULL;
	GConfValue *val_proxy_port = NULL;
	GError *err_gconf = NULL;

	val_use = gconf_engine_get (gl_gconf_engine, KEY_GCONF_USE_HTTP_PROXY, &err_gconf);
	val_proxy_host = gconf_engine_get (gl_gconf_engine, KEY_GCONF_HTTP_PROXY_HOST, &err_gconf);
	val_proxy_port = gconf_engine_get (gl_gconf_engine, KEY_GCONF_HTTP_PROXY_PORT, &err_gconf);

	if ( NULL != val_use && GCONF_VALUE_BOOL == val_use->type
		&& NULL != val_proxy_host
		&& GCONF_VALUE_STRING == val_proxy_host->type
		&& 0 != strlen (gconf_value_get_string (val_proxy_host))
		&& gconf_value_get_bool (val_use)
	) {
		g_free (config.upstream_proxy);
		config.upstream_proxy = NULL;

		config.upstream_proxy = g_strdup (gconf_value_get_string (val_proxy_host));

		if (NULL != val_proxy_port
			&& GCONF_VALUE_INT == val_proxy_port->type
			&& 0xffff >= (unsigned)gconf_value_get_int (val_proxy_port)
		) {
			config.upstream_port = (unsigned) gconf_value_get_int (val_proxy_port);
		} else {
			config.upstream_port = (unsigned) PORT_HTTP_PROXY_DEFAULT;
		}

		log("HTTP proxy changed to '%s:%d'", 
			config.upstream_proxy, 
			config.upstream_port
		);
	} else {
		g_free (config.upstream_proxy);
		config.upstream_proxy = NULL;
		log ("HTTP proxy changed to <none>");
	}
}


static void 
load_gconf_defaults (void)
{
	static const UtilGConfWatchVariable watchvar_use_http_proxy =
		{KEY_GCONF_USE_HTTP_PROXY, GCONF_VALUE_BOOL, {NULL}, watch_gconf_proxy_cb};

	static const UtilGConfWatchVariable watchvar_http_proxy_host =
		{KEY_GCONF_HTTP_PROXY_HOST, GCONF_VALUE_STRING, {NULL}, watch_gconf_proxy_cb};

	static const UtilGConfWatchVariable watchvar_http_proxy_port =
		{KEY_GCONF_HTTP_PROXY_PORT, GCONF_VALUE_INT, {NULL}, watch_gconf_proxy_cb};

	if ( ! config.specified_upstream_proxy ) {
		util_gconf_watch_variable (&watchvar_use_http_proxy);
		util_gconf_watch_variable (&watchvar_http_proxy_host);
		util_gconf_watch_variable (&watchvar_http_proxy_port);
	}

	gl_gconf_ssl_cert = util_gconf_get_ensure_string (KEY_GCONF_DEFAULT_SSL_CERT);
	gl_gconf_ssl_cert_dir = util_gconf_get_ensure_string (KEY_GCONF_DEFAULT_SSL_CERT_DIR);

	gl_gconf_ssl_disabled = util_gconf_get_ensure_bool (KEY_GCONF_DISABLE_SSL, FALSE);
}

#endif /*SIMPLE_PROXY*/

/*
 * If we're running in -t mode, we need to translate DAV
 * Destination: headers
 * (just like in impl-eazel-proxy.c:user_proxy_request_cv
 */
static gpointer /* ProxyRequestCb */
simple_proxy_request_cb (gpointer user_data, 
			 unsigned short port, 
			 HTTPRequestLine *request, 
			 GList **p_header_list)
{
	GList * destination_node;

	
	if ( ! config.target_path ) {
		return NULL;
	}

	destination_node = g_list_find_custom (*p_header_list, "Destination: ", util_glist_string_starts_with_case_insensitive);

	if (destination_node) {
		char *new_header;
		char *dest_url;
		HTTPRequestLine *target_request;
		HTTPRequestLine *dest_request;

		dest_url = (char *)(destination_node->data) + strlen ("Destination: ");

		dest_request = request_new();
		target_request = request_new();
		
		if ( request_parse_url (config.target_path, target_request ) 
			&& request_parse_url (dest_url, dest_request)
			&& dest_request->host 
			&& (0 == strcmp ("localhost", dest_request->host) || 0 == strcmp("127.0.0.1", dest_request->host))
			&& dest_request->port == port
		) {
			new_header = g_strdup_printf ("Destination: %s://%s:%d%s",
					dest_request->uri,
					target_request->host,
					target_request->port,
					dest_request->path);
			g_free (destination_node->data);
			destination_node->data = new_header;
		}
		
		request_free (target_request);
		request_free (dest_request);
	}


	return NULL;
}

/* If the user has specified --proxy-auth, apply it */
static int
simple_proxy_auth_cb (	gpointer user_data, 
			unsigned short port, 
			HTTPRequestLine *request, 
			GList **p_header_list,
			/*OUT*/ char **p_status_text,
			/*OUT*/ GList **p_ret_header_list)
{
	GList *auth_header_node;
	char *encoded_credentials = NULL;

	if ( NULL == gl_opt_proxy_auth ) {
		return 200;
	}

	auth_header_node = g_list_find_custom (*p_header_list, "Proxy-Authorization: ", util_glist_string_starts_with_case_insensitive);

	if (NULL != auth_header_node) {
		gchar **header_split;
		encoded_credentials = util_base64_encode (gl_opt_proxy_auth);

		header_split = g_strsplit ((char *) auth_header_node->data, " ", 3);

		if (NULL != header_split[0]
			&& NULL != header_split[1] 
			&& NULL != header_split[2]
			&& 0==strcasecmp (header_split[1], "Basic")
			&& 0==strcasecmp (header_split[2], encoded_credentials))
		{
			/* Remove the Proxy-Authorization header node */
			g_free (auth_header_node->data); 
			*p_header_list = g_list_remove_link (*p_header_list, auth_header_node);
			g_list_free (auth_header_node);		


			g_strfreev (header_split);
			g_free (encoded_credentials);
			return 200;
		}
		g_strfreev (header_split);
		g_free (encoded_credentials);
	}

	/* Otherwise, demand credentials */
	/* Note that excluding "realm" will break Mozilla (at least 0.8) */
	*p_status_text = g_strdup ("Proxy Authentication Required");
	*p_ret_header_list = g_list_prepend (*p_ret_header_list, g_strdup ("Proxy-Authenticate: Basic realm=\"ammonite\""));
	return 407; 	/* 407 Proxy Authentication Required */
}

int main (int argc, 
	  char **argv)
{
	gboolean godaemon = TRUE;
	char *p;
	int port;
	int i;
	char *mode;
	int option;
	poptContext  popt_context;

#ifdef CHECK_FOR_LEAKS
	leak_checker_init (argv[0]);
//	g_atexit (leak_checker_dump);
#endif

	memset (&stats, 0, sizeof(struct stat_s));

	popt_context = poptGetContext ("eazel-proxy", argc, (const char **)argv, popt_options, 0);
	while ((option = poptGetNextOpt (popt_context)) >= 0) {
		switch (option) {
		case 'h':
			poptPrintUsage (popt_context, stderr, 0);
			exit (EX_OK);
			break;
		case 'v':
			display_version ();
			exit (EX_OK);
			break;
		case 'l':
                        g_free (config.logf_name);
			config.logf_name = g_strdup (poptGetOptArg (popt_context));
			break;
		case 'n':
			config.use_oaf = FALSE;
			break;
                case 't':
                        g_free (config.target_path);
                        config.target_path = g_strdup (poptGetOptArg (popt_context));
			if (strlen (config.target_path) == 0) {
				g_free (config.target_path);
				config.target_path = NULL;
			}

			if ( ! util_validate_url (config.target_path)) {
				fprintf (stderr, "-t argument must be url (was '%s')\n", config.target_path);
				exit (-1);
			}
                        break;
		case 'u':
			config.specified_upstream_proxy = TRUE;
			g_free (config.upstream_proxy);
			config.upstream_proxy = g_strdup (poptGetOptArg (popt_context));
			if (strlen (config.upstream_proxy) == 0) {
				config.upstream_proxy = NULL;
				break;
			}

			p = strchr (config.upstream_proxy, ':');
			if (p) {
				config.upstream_port = atoi (p+1);
				*p = 0;
			} else {
				config.upstream_port = PORT_HTTP_DEFAULT;
			}
			break;
		case 'L':
			config.append_log = TRUE;
			break;
#ifndef NO_DEBUG_MIRRORING
		case 'd':
			godaemon = FALSE;
			break;
		case 'r':
			config.check_uid = FALSE;
			break;
		case 'b':
			config.local_host = g_strdup (poptGetOptArg (popt_context));
			break;
		case 'p':
			config.local_port = atoi (poptGetOptArg (popt_context));
			break;
		case 'e':
			config.use_stderr = TRUE;
			break;
		case 'm':
			config.mirror_log_dir = g_strdup (poptGetOptArg (popt_context));
			break;
#endif
		}
	}

	if (config.logf_name && (config.logf_name[0] == '~') && (config.logf_name[1] == '/')) {
		char *oldlog = config.logf_name;

		/* replace leading ~/ with the user's homedir */
		config.logf_name = g_strjoin ("/", g_get_home_dir(), oldlog + 2, NULL);
		g_free (oldlog);
	}

	if (config.use_stderr) {
		config.logf = stderr;
		godaemon = FALSE;
	} else if (config.logf_name) {
		mode = (config.append_log ? "a" : "w");

		if (!(config.logf = fopen (config.logf_name, mode))) {
			fprintf (stderr, "Unable to open logfile %s for writing!\n",
				 config.logf_name);
			exit (EX_CANTCREAT);
		}
	}

#ifdef HAVE_OPENSSL

	/* strdup it before I free the context */
	if (gl_opt_certfile) {
		gl_opt_certfile = g_strdup (gl_opt_certfile);
	}

	if (gl_opt_cert_dir) {
		gl_opt_certfile = g_strdup (gl_opt_cert_dir);
	}

#endif

	poptFreeContext (popt_context);
	
	if (godaemon) {
		make_daemon ();
	}

#ifndef SIMPLE_PROXY
	if (config.use_oaf) {
		CORBA_ORB orb;

		/* Init CORBA and OAF */
		orb = oaf_init (argc, argv);

		if (NULL == orb) {
			log("Unable to init OAF");
			exit (-1);
		}

		util_gconf_init ();
		load_gconf_defaults();

#ifdef HAVE_OPENSSL
		if (! gl_opt_no_ssl && !gl_gconf_ssl_disabled) {
			config.use_ssl = TRUE;
			/* FIXME failure here appears to be fatal; should be more graceful */

			if (gl_opt_certfile) {
				log ("Enabling SSL; using certfile %s", gl_opt_certfile);
				eazel_init_ssl (NULL, gl_opt_certfile);
			} else if (gl_opt_cert_dir) {
				log ("Enabling SSL; using cert directory %s", gl_opt_cert_dir);
				eazel_init_ssl (gl_opt_cert_dir, NULL);
			} else if (gl_gconf_ssl_cert) {
				log ("Enabling SSL; using certfile %s", gl_gconf_ssl_cert);
				eazel_init_ssl (NULL, gl_gconf_ssl_cert);
			} else if (gl_gconf_ssl_cert_dir) {
				log ("Enabling SSL; using cert directory %s", gl_gconf_ssl_cert);
				eazel_init_ssl (NULL, gl_gconf_ssl_cert);
			} else {
				log ("Enabling SSL; using cert directory %s", DATADIR EAZEL_DEFAULT_CERT_DIR);
				eazel_init_ssl (DATADIR EAZEL_DEFAULT_CERT_DIR, NULL );
			}
			ssl_set_preferred_cipher_list (util_gconf_get_ensure_string (KEY_GCONF_SSL_CIPHER));

			/* WARNING: use of this option significantly compromises security */
			if (util_gconf_get_ensure_bool (KEY_GCONF_SSL_IGNORE_SERVER_CERT, FALSE)) {
				ssl_set_ignore_server_cert (TRUE);
			}

		}
#endif
		/* Exits on failure */
		init_impl_eazelproxy (orb);
	}
#endif /*SIMPLE_PROXY*/

	if (signal(SIGPIPE, SIG_IGN) == SIG_ERR) {
		fprintf(stderr, "Could not set SIGPIPE\n");
		exit(EX_OSERR);
	}
	if (signal(SIGUSR1, takesig) == SIG_ERR) {
		fprintf(stderr, "Could not set SIGUSR1\n");
		exit(EX_OSERR);
	}
	if (signal(SIGTERM, takesig) == SIG_ERR) {
		fprintf(stderr, "Could not set SIGTERM\n");
		exit(EX_OSERR);
	}
	if (signal(SIGHUP, takesig) == SIG_ERR) {
		fprintf(stderr, "Could not set SIGHUP\n");
		exit(EX_OSERR);
	}

	log(PACKAGE " " VERSION " starting...");

#ifndef SIMPLE_PROXY
	/* If we're using OAF, we'll wait to be directed to listen to a port */
	if (!config.use_oaf)
#endif /* SIMPLE_PROXY */
	{
		static const ProxyCallbackInfo callbacks = {
			simple_proxy_request_cb,
			NULL,
			NULL,
			simple_proxy_auth_cb
		};

		port = proxy_listen (config.local_host, config.local_port, NULL, &callbacks, NULL);
		if ( 0 == port ) {
			exit (EX_UNAVAILABLE);
		}
		log("listening on port %d", port);
#ifndef SIMPLE_PROXY
	} else {
		log ("Waiting for EazelProxy::UserControl::authenticate_user");
#endif /* SIMPLE_PROXY */
	}

	g_main_set_poll_func(socket_glib_poll_func);

	l_main_loop = g_main_new (TRUE);

	while (g_main_is_running(l_main_loop)) {
		g_main_iteration (TRUE);
		socket_event_pump ();
	}

	log("shutting down...");

	/* it's nice to call the event loop a few more times, to try to let any last
	 * buffered output get shoved out...
	 */
	for (i = 0; i < 10; i++) {
		g_main_iteration (FALSE);
	}

#ifndef SIMPLE_PROXY
	if (config.use_oaf) {
		shutdown_impl_eazelproxy();
	}
#endif /*SIMPLE_PROXY*/

	g_main_destroy (l_main_loop);

	log(PACKAGE " " VERSION " terminated.");
	if (config.logf && (config.logf != stderr)) {
		fclose(config.logf);
	}

#ifdef CHECK_FOR_LEAKS
	leak_checker_dump();
#endif

	exit(EX_OK);
}
