# Note this is NOT a relocatable thing :)
%define name		ammonite
%define ver		@VERSION@
%define RELEASE		0_cvs_0
%define rel		%{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}
%define prefix		/usr
%define sysconfdir	/etc

Name:		%name
Summary:	Ammonite is a portion of the Eazel Services authentication framework.
Version: 	%ver
Distribution:	GNOME
Vendor:		CVS
Release: 	%rel
Copyright: 	GPL
Group:		User Interface/Desktop
Source: 	%{name}-%{ver}.tar.gz
URL: 		http://nautilus.eazel.com/
BuildRoot:	/var/tmp/%{name}-%{ver}-root
Docdir: 	%{prefix}/doc
Requires:	gnome-vfs >= 0.3.1
#Requires:	openssl
# Requires:	openssl >= 0.9.5
# it really does require that version, but we would rather have
# ammonite break than break a users ssh so we are putting no version
# requirement in so we can distribute and rsa free version

%description
Ammonite is a non-caching client-side HTTP proxy with a set of special features 
required by Eazel to communicate with Eazel Services.  Ammonite provides the user authentication and encryption features used by Eazel Services.  It is part of the
GNOME project, and its source code can be found in the GNOME CVS repository.

You will need to install Ammonite if you intend to use Eazel Services.

Ammonite links statically with OpenSSL, which is (C) Eric Young.

Ammonite was originally based on TinyProxy v1.3, written by by Steven Young 
<sdyoung@well.com> and Robert James Kaes <rjkaes@flarenet.com>.

%package devel
Summary:	Libraries and include files for developing Ammonite clients
Group:		Development/Libraries
Requires:	%name = %{PACKAGE_VERSION}
Obsoletes:	%{name}-devel

%description devel
This package provides the necessary development libraries and include
files to allow you to develop components that make use of the Ammonite authentication
services.  You will need to install this package if you intend to build Nautilus
from source code.

%changelog
* Wed Apr 26 2000 Robin * Slomkowski <rsllomkow@eazel.com>
- created this thing
* Wed Apr 26 2000 Mike Fleming <mfleming@eazel.com>
- Small edits of description fields

%prep
%setup

%build
%ifarch alpha
  MYARCH_FLAGS="--host=alpha-redhat-linux"
%endif

LC_ALL=""
LINGUAS=""
LANG=""
export LC_ALL LINGUAS LANG

CFLAGS="$RPM_OPT_FLAGS" ./configure $MYARCH_FLAGS --prefix=%{prefix} \
	--sysconfdir=%{sysconfdir}

if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{prefix} sysconfdir=$RPM_BUILD_ROOT%{sysconfdir} install

for FILE in "$RPM_BUILD_ROOT/bin/*"; do
	file "$FILE" | grep -q not\ stripped && strip $FILE
done

%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

%post
if ! grep %{prefix}/lib /etc/ld.so.conf > /dev/null ; then
  echo "%{prefix}/lib" >> /etc/ld.so.conf
fi
  
/sbin/ldconfig
  
%postun -p /sbin/ldconfig

%files

%defattr(0555, bin, bin)
%doc AUTHORS COPYING ChangeLog NEWS README
%{prefix}/bin/eazel-proxy
%{prefix}/bin/eazel-proxy-util
%{prefix}/lib/*.sh

%defattr (0444, bin, bin)
%config %{sysconfdir}/vfs/modules/*.conf
%{prefix}/share/oaf/*.oaf
%{prefix}/share/nautilus/certs/*.pem
%{prefix}/share/idl/*.idl
%{prefix}/share/pixmaps/nautilus/*.png

%files devel

%defattr(0555, bin, bin)
%{prefix}/lib/*.a

%defattr(0444, bin, bin)
%{prefix}/include/libtrilobite/*.h
