/* $Id$
 * 
 * eazel-proxy-util-gtk: GTK-dependent portions of eazel-proxy-util
 *
 * Copyright (C) 2000 Eazel, Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author:  Michael Fleming <mfleming@eazel.com>
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include "eazel-proxy-util-gtk.h"
#include <stdio.h>
#include <gnome.h>
#include <ammonite-login-dialog.h>
#include <libammonite.h>
#include <unistd.h>
#include <signal.h>

extern PortableServer_POA gl_poa; /*in eazel-proxy-util.c*/

#if 0
void
eazel_proxy_util_do_dialog (void)
{
	GtkWidget *dialog;
	gboolean dialog_return;
	char * my_argv[] = {"eazel-proxy-util", NULL};
	int err;

	err = gnome_init("eazel-proxy-util", VERSION, 1, my_argv);

	dialog = ammonite_login_dialog_new ( 
			"Eazel Services Login", 
			"Please log into Eazel Services", 
			"", 
			"", 
			FALSE,
			FALSE
		 );

	dialog_return = ammonite_login_dialog_run_and_block (AMMONITE_LOGIN_DIALOG (dialog));

	if (dialog_return) {
		fprintf (stderr, "User name '%s' passwd '%s'\n", 
			ammonite_login_dialog_get_username (AMMONITE_LOGIN_DIALOG (dialog)),
			ammonite_login_dialog_get_password (AMMONITE_LOGIN_DIALOG (dialog))
		);
	} else {
		fprintf (stderr, "User cancelled...\n");
	}	
}
#endif /* 0 */

#ifdef ENABLE_REAUTHN
static GMainLoop * gl_reauthn_main_loop = NULL;

static void
reauthn_handle_signal (int sig)
{
	switch (sig) {
	case SIGINT:
	case SIGHUP:
	case SIGTERM:
#ifdef DEBUG
		fprintf (stderr, "Received signal, leaving...\n");
#endif
		exit (0);
		/* FIXME bugzilla.eazel.com 2850:
		 * why doesn't this cause the event loop to quit?
		 */
		/*g_main_quit( gl_reauthn_main_loop );*/
		break;
	}
}

static CORBA_boolean
reauthenticate_user_cb (
	const EazelProxy_User *user, 
	const CORBA_long retry_count, 
	CORBA_char **password, 
	gpointer user_data, 
	CORBA_Environment *ev
) {
	GtkWidget *dialog;
	gboolean dialog_return;
	const char *message;
	gboolean success = FALSE;

	/*FIXME bugzilla.eazel.com 2849: these strings need to be i18n'able */
	if ( 0 == retry_count ) {
		message="Your Eazel Services session has expired.  Please log in again";
	} else {
		message="The password you entered for Eazel Services was incorrect.  Please try again.";
	}

	dialog = ammonite_login_dialog_new ( 
			"Eazel Services Login", 
			message, 
			user->user_name, 
			"", 
			TRUE,
			FALSE
		 );

	dialog_return = ammonite_login_dialog_run_and_block (AMMONITE_LOGIN_DIALOG (dialog));

	if (dialog_return) {
		char *tmp_password;

		tmp_password = ammonite_login_dialog_get_password (AMMONITE_LOGIN_DIALOG (dialog));
		if ( NULL != tmp_password ) {
			*password = CORBA_string_dup (tmp_password);
			g_free (tmp_password);
			success = TRUE;
		}		
	}	

	return success;
}

void
eazel_proxy_util_do_reauthn_listen (EazelProxy_UserControl cntl)
{

	CORBA_Environment ev;
	EazelProxy_UserListener user_listener;
	EazelProxy_ListenerToken token;
	char * my_argv[] = {"eazel-proxy-util", NULL};
	int err;

	AmmoniteUserListenerWrapperFuncs cb_funcs = {
		NULL,
		NULL,
		reauthenticate_user_cb,
		NULL,
	};

#ifdef DEBUG
	fprintf (stderr, "In doing --reauthn-listen\n");
#endif
	CORBA_exception_init (&ev);

	err = gnome_init("eazel-proxy-util", VERSION, 1, my_argv);

	if ( 0 != err ) {
		fprintf (stderr, "Error calling gnome_init\n");
		exit (-1);
	}
	
	user_listener = ammonite_user_listener_wrapper_new (gl_poa, &cb_funcs, NULL);

	if (CORBA_OBJECT_NIL == user_listener) {
		fprintf (stderr, "Couldn't create AuthnCallback\n");
		exit (-1);
	}

	token = EazelProxy_UserControl_add_listener (cntl, user_listener, &ev);

	if (CORBA_NO_EXCEPTION != ev._major) {
		fprintf (stderr, "Exception during add_listener\n");
	} else {
		void * old_sigterm, * old_sighup, *old_sigint;
		
		old_sigint = signal(SIGINT, reauthn_handle_signal);
		old_sigterm = signal(SIGTERM, reauthn_handle_signal);
		old_sighup = signal(SIGHUP, reauthn_handle_signal);

		gl_reauthn_main_loop = g_main_new (FALSE);

		g_main_run (gl_reauthn_main_loop);		

		g_main_destroy (gl_reauthn_main_loop);

		gl_reauthn_main_loop = NULL;		

		signal(SIGINT, old_sigint);
		signal(SIGTERM, old_sigterm);
		signal(SIGHUP, old_sighup);
	}

	EazelProxy_UserControl_remove_listener (cntl, token, &ev);

	ammonite_user_listener_wrapper_free (gl_poa, user_listener);

#ifdef DEBUG
	fprintf (stderr, "Leaving do_reauthn_listen\n");
#endif

	CORBA_exception_free (&ev);
}

#endif /* ENABLE_REAUTHN */

#if 0
EazelProxy_User *
eazel_proxy_util_login_dialog (EazelProxy_UserControl cntl, char * username)
{
	static gboolean is_gnome_inited = FALSE;

	GtkWidget *dialog;
	gboolean dialog_return;

	if (!is_gnome_inited) {
		char * my_argv[] = {"eazel-proxy-util", NULL};
		int err;

#ifdef DEBUG
		fprintf (stderr, "Initing GNOME\n");
#endif /* DEBUG */

		if (NULL == g_getenv ("DISPLAY")) {
#ifdef DEBUG
			fprintf (stderr, "No DISPLAY variable, not opening login dialog\n");
#endif
			return NULL;
		}

		err = gnome_init("eazel-proxy-util", VERSION, 1, my_argv);

		if ( 0 != err ) {
#ifdef DEBUG
			fprintf (stderr, "couldn't init GNOME\n");
#endif
			return NULL;
		}

		is_gnome_inited = TRUE;
	}

#ifdef DEBUG
	fprintf (stderr, "Opening Dialog\n");
#endif /* DEBUG */

	dialog = ammonite_login_dialog_new ( 
			"Eazel Services Login", 
			"Please log into Eazel Services", 
			"", 
			"", 
			FALSE,
			FALSE
		 );

	dialog_return = ammonite_login_dialog_run_and_block (AMMONITE_LOGIN_DIALOG (dialog));

	if (dialog_return) {
		char *username;
		char *passwd;

		username = ammonite_login_dialog_get_username (AMMONITE_LOGIN_DIALOG (dialog));
		passwd = ammonite_login_dialog_get_password (AMMONITE_LOGIN_DIALOG (dialog));

#ifdef DEBUG
		fprintf (stderr, "Authn'ing user\n");
#endif /* DEBUG */
		
		return eazel_proxy_util_do_login (cntl, username, passwd, NULL, NULL, NULL);
	} else {
		fprintf (stderr, "User cancelled...\n");
	}

	/* Clean up events after the dialog */
	while (gtk_events_pending()) {
		gtk_main_iteration();
	}

	gtk_widget_destroy (dialog);

	return NULL;
}


static gboolean
try_init_gnome ()
{
	static gboolean is_gnome_inited = FALSE;

	if (!is_gnome_inited) {
		char * my_argv[] = {"eazel-proxy-util", NULL};
		int err;

#ifdef DEBUG
		fprintf (stderr, "Initing GNOME\n");
#endif /* DEBUG */

		if (NULL == g_getenv ("DISPLAY")) {
#ifdef DEBUG
			fprintf (stderr, "No DISPLAY variable, not opening login dialog\n");
#endif
			return FALSE;
		}

		err = gnome_init("eazel-proxy-util", VERSION, 1, my_argv);

		if ( 0 != err ) {
#ifdef DEBUG
			fprintf (stderr, "couldn't init GNOME\n");
#endif
			return FALSE;
		}

		is_gnome_inited = TRUE;
	}

	return TRUE;
}

gboolean
eazel_proxy_util_do_prompt_dialog (
	const char *user, 
	const char *pw, 
	gboolean retry, 
	char **p_user, 
	char **p_pw
) {

	GtkWidget *dialog;
	gboolean dialog_return;

	g_return_val_if_fail ( NULL != user || NULL != p_user, FALSE);
	g_return_val_if_fail ( NULL != p_pw, FALSE);

	if (p_user) {
		*p_user = NULL;
	}
	*p_pw = NULL;

	if (!try_init_gnome ()) {
		return FALSE;
	}

#ifdef DEBUG
	fprintf (stderr, "Opening Dialog\n");
#endif /* DEBUG */

	dialog = ammonite_login_dialog_new ( 
			"Eazel Services Login", 
			retry 	? "Please log into Eazel Services"
				: "You username or password was incorrect.  Please try again.", 
			"", 
			"", 
			FALSE,
			FALSE
		 );

	if ( NULL != user && '\0' != user[0] ) {
		ammonite_login_dialog_set_username ( 
			AMMONITE_LOGIN_DIALOG (dialog),
			user
		);
		ammonite_login_dialog_set_readonly_username ( 
			AMMONITE_LOGIN_DIALOG (dialog),
			TRUE
		);
	}

	dialog_return = ammonite_login_dialog_run_and_block (AMMONITE_LOGIN_DIALOG (dialog));

	if (dialog_return) {
		if ( NULL != p_user ) {
			*p_user = ammonite_login_dialog_get_username (AMMONITE_LOGIN_DIALOG (dialog));
		}
		if ( NULL != p_pw ) {
			*p_pw = ammonite_login_dialog_get_password (AMMONITE_LOGIN_DIALOG (dialog));		
		}
	} else {
#ifdef DEBUG
		fprintf (stderr, "User cancelled...\n");
#endif /* DEBUG */
	}

	gtk_widget_destroy (dialog);

	/* Clean up events after the dialog */
	while (gtk_events_pending()) {
		gtk_main_iteration();
	}


	return dialog_return;
}

void
eazel_proxy_util_do_error_dialog ()
{
	GtkWidget *dialog;

	if (!try_init_gnome()) {
		return;
	}

#ifdef DEBUG
	fprintf (stderr, "Opening Dialog\n");
#endif /* DEBUG */

	dialog = gnome_error_dialog ("I'm sorry, your Eazel Service username or password are incorrect.");

	gnome_dialog_run_and_close (GNOME_DIALOG (dialog));

	/* Clean up events after the dialog */
	while (gtk_events_pending()) {
		gtk_main_iteration();
	}
}

void
eazel_proxy_util_do_network_error_dialog ()
{
	GtkWidget *dialog;

	if (!try_init_gnome()) {
		return;
	}

#ifdef DEBUG
	fprintf (stderr, "Opening Network Error Dialog\n");
#endif /* DEBUG */

	dialog = gnome_error_dialog ("I'm sorry, network problems are preventing you from connecting to Eazel Services.");

	gnome_dialog_run_and_close (GNOME_DIALOG (dialog));

	/* Clean up events after the dialog */
	while (gtk_events_pending()) {
		gtk_main_iteration();
	}
}

#endif /* 0 */

