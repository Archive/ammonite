AC_INIT()

AMMONITE_MAJOR_VERSION=1
AMMONITE_MINOR_VERSION=0
AMMONITE_MICRO_VERSION=2
AMMONITE_VERSION="$AMMONITE_MAJOR_VERSION.$AMMONITE_MINOR_VERSION.$AMMONITE_MICRO_VERSION"
AM_INIT_AUTOMAKE(ammonite,$AMMONITE_VERSION)
AM_CONFIG_HEADER(src/config.h)

AC_SUBST(AMMONITE_MAJOR_VERSION)
AC_SUBST(AMMONITE_MINOR_VERSION)
AC_SUBST(AMMONITE_MICRO_VERSION)
AC_SUBST(AMMONITE_VERSION)

dnl Checks for programs.
AM_DISABLE_STATIC
AM_PROG_LIBTOOL
AC_PROG_CC
AC_PROG_INSTALL
AC_ISC_POSIX

AM_PROG_XML_I18N_TOOLS

dnl Checks for header files.
AC_HEADER_STDC
AC_CHECK_HEADERS(fcntl.h sys/time.h syslog.h unistd.h)

dnl
dnl Python
dnl

AM_PATH_PYTHON(module)
AM_CHECK_PYTHON_HEADERS(,[PYTHON_INCLUDES=""; AC_MSG_RESULT(could not find Python headers)])

if test -z "$PYTHON_INCLUDES"; then
PYTHON_BUILD_DIRS=""
else
PYTHON_BUILD_DIRS="python"
fi
AC_SUBST(PYTHON_BUILD_DIRS)

dnl Checks for typedefs, structures, and compiler characteristics.
dnl AC_C_CONST
dnl AC_TYPE_SIZE_T
dnl AC_HEADER_TIME
dnl AC_C_INLINE

dnl ====================================
dnl =
dnl = Profiling support
dnl =
dnl ====================================
ENABLE_PROFILER=
AC_ARG_ENABLE(profiler,
[  --enable-profiler		Enable profiler],
ENABLE_PROFILER=1
AC_DEFINE(ENABLE_PROFILER))

if test "x$ENABLE_PROFILER" = "x1"
then
	CFLAGS="-g -O -gdwarf-2 -finstrument-functions -D__NO_STRING_INLINES"
	LDFLAGS="/gnome/PROFILE/lib/libprofiler.so -lpthread"
fi

AC_SUBST(ENABLE_PROFILER)
AM_CONDITIONAL(ENABLE_PROFILER, test "x$ENABLE_PROFILER" = "x1")
dnl ====================================
dnl = Profiling support
dnl ====================================

dnl borrowed from stunnel - check for openssl
checkssldir() { :
    if test -f "$1/include/openssl/ssl.h"; then
        AC_DEFINE(HAVE_OPENSSL)
        ssldir="$1"
        return 0
    fi
    return 1
}

AC_MSG_CHECKING([for openSSL directory])
AC_ARG_WITH(openssl,
    [  --with-openssl=DIR      location of installed openSSL libraries/include files])

dnl Search default localtions of SSL library
for maindir in $with_openssl /usr /usr/local /usr/lib /usr/pkg ; do
	for dir in $maindir $maindir/openssl $maindir/ssl; do
		checkssldir $dir
	done
done

if test -z "$ssldir"; then
    AC_MSG_RESULT(Not found)
    echo
    echo "Building without openSSL!"
    echo "Use --with-openssl option if you want SSL support"
    echo
    OPENSSL_LIBS=
    OPENSSL_CFLAGS=
else

    OPENSSL_LIBS="-L$ssldir/lib -lssl -lcrypto"

    OPENSSL_CFLAGS="-I$ssldir/include"
fi
AC_MSG_RESULT($ssldir)
dnl AC_DEFINE_UNQUOTED(OPENSSL_LIBS, "$OPENSSL_LIBS")
dnl AC_DEFINE_UNQUOTED(OPENSSL_CFLAGS, "$OPENSSL_CFLAGS")
AC_SUBST(HAVE_OPENSSL)
AC_SUBST(OPENSSL_LIBS)
AC_SUBST(OPENSSL_CFLAGS)


dnl chris - allow user to choose log file location, port and username
AC_ARG_WITH(log-file, \
  [--with-log-file=FILE    Default logfile name], \
	[AC_DEFINE_UNQUOTED(DEFAULT_LOG, "$withval" )])

dnl mfleming -- added GLIB
AM_PATH_GLIB(1.2.8,,
AC_MSG_ERROR([
*** GLIB 1.2.8 or better is required. The latest version of GLIB
*** is always available from ftp://ftp.gtk.org/.]))
AM_PATH_GCONF(0.11.0,,,gconf)

AC_PATH_PROG(XML_CONFIG,xml-config,no)
if test x$XML_CONFIG = xno; then
  AC_MSG_ERROR(Couldn't find xml-config please install the gnome-xml package)
fi
XML_LIBS=`$XML_CONFIG --libs`
XML_CFLAGS=`$XML_CONFIG --cflags`
AC_SUBST(XML_LIBS)
AC_SUBST(XML_CFLAGS)

AM_PATH_OAF(0.6.0)

dnl This is only for the login box
AM_PATH_GNOME(1.2.0,,AC_MSG_ERROR([*** GNOME 1.2.0 or better is required.]), gtk gnome gnomeui)
AC_SUBST(GTK_LIBS)
AC_SUBST(GTK_CFLAGS)
AC_SUBST(GNOME_LIBS)
AC_SUBST(GNOME_CFLAGS)
AC_SUBST(GNOMEUI_LIBS)
AC_SUBST(GNOMEUI_CFLAGS)


AC_CHECK_LIB(popt, poptGetContext,,
AC_MSG_ERROR([You must have popt installed. Get it from ftp://ftp.redhat.com/pub/redhat/code/popt/]))

dnl Checks for library functions.
dnl AC_TYPE_SIGNAL
dnl AC_FUNC_STRFTIME
dnl AC_FUNC_VPRINTF
dnl AC_CHECK_FUNCS(socket select strerror strdup vsyslog vsnprintf)

dnl Add the warnings if we have the GCC compiler
rm -f conftest*

dnl Checks for i18n
ALL_LINGUAS="az ca da de el es eu fi fr gl hu it ja ko no pl pt pt_BR ro ru sk sv tr uk zh_CN zh_TW"
AM_GNOME_GETTEXT
# AM_GNOME_GETTEXT above substs $DATADIRNAME
# this is the directory where the *.{mo,gmo} files are installed
gnomelocaledir='${prefix}/${DATADIRNAME}/locale'
AC_SUBST(gnomelocaledir)


AC_ARG_ENABLE(more-warnings,
[  --enable-more-warnings  Maximum compiler warnings],
set_more_warnings="$enableval",[
if test -f $srcdir/CVSVERSION; then
	set_more_warnings=yes
else
	set_more_warnings=no
fi
])
AC_MSG_CHECKING(for more warnings, including -Werror)
if test "$GCC" = "yes" -a "$set_more_warnings" != "no"; then
	AC_MSG_RESULT(yes)
	CFLAGS="\
	-Wall \
	-Wchar-subscripts -Wmissing-declarations -Wmissing-prototypes \
	-Wnested-externs -Wpointer-arith \
	$CFLAGS"

	for option in -Wsign-promo -Wno-sign-compare; do
		SAVE_CFLAGS="$CFLAGS"
		CFLAGS="$CFLAGS $option"
		AC_MSG_CHECKING([whether gcc understands $option])
		AC_TRY_COMPILE([], [int test;],
			has_option=yes,
			has_option=no,)
		CFLAGS="$SAVE_CFLAGS"
		if test $has_option = yes; then
		  CFLAGS="$CFLAGS $option"
		fi
		AC_MSG_RESULT($has_option)
		unset has_option
		unset SAVE_CFLAGS
	done
	unset option
else
	AC_MSG_RESULT(no)
fi

dnl ORBit
AM_PATH_ORBIT


AC_CHECK_LIB(nsl, gethostname, [LIBS="$LIBS -lnsl"])
AC_CHECK_LIB(socket, setsockopt, [LIBS="$LIBS -lsocket"])

dnl Check to see if the debuging code is turned on
AC_MSG_CHECKING(whether to include debugging code)
AC_ARG_ENABLE(debug, \
  [--enable-debug=[yes/no]          turn on additional debugging code],
  [debug_enabled=yes], [debug_enabled=no])
if test "$debug_enabled" = "yes"; then
  CFLAGS="$CFLAGS -DDEBUG"
fi
AC_MSG_RESULT($debug_enabled)

dnl Check for SOCKS support
AC_MSG_CHECKING(whether to include support for SOCKS)
AC_ARG_ENABLE(socks, \
  [--enable-socks=[yes/no]          enable SOCKS support],
  [socks_enabled=yes], [socks_enabled=no])
if test "$socks_enabled" = "$yes"; then
  AC_CHECK_HEADER(socks.h, [socks_header="yes"], [socks_header="no"])
  AC_CHECK_LIB(socks, main, [socks_library="yes"], [socks_library="no"])
  if test "$socks_header" = "yes" && test "$socks_library" = "yes"; then
    CFLAGS="$CFLAGS -I/usr/include/sock.h -DSOCKS"
    LIBS="$LIBS -lsocks"
  else
    socks_enabled=no
  fi
fi
AC_MSG_RESULT($socks_enabled)

AC_SUBST(IDL_CFLAGS)

AC_SUBST(CFLAGS)dnl
AC_SUBST(LIBS)dnl

dnl
dnl Variables used for ammoniteConf.sh
dnl

AMMONITE_LIBDIR='-L${libdir}'
AMMONITE_INCLUDEDIR='-I${includedir}'
AMMONITE_LIBS='-lammonite-gtk -lammonite'

AC_SUBST(AMMONITE_LIBDIR)
AC_SUBST(AMMONITE_INCLUDEDIR)
AC_SUBST(AMMONITE_LIBS)

AC_OUTPUT([
ammonite.spec
Makefile
libuuid/Makefile
idl/Makefile
src/Makefile
doc/Makefile
src/util/Makefile
libammonite/Makefile
libammonite-gtk/Makefile
python/Makefile
data/Makefile
po/Makefile.in
])

dnl <= Configuration summary =>

echo "<= ammonite configuration summary :"
dnl <= Profile support? =>
case "X$ENABLE_PROFILER" in
X1)
echo "
Enable profiler         : YES"
;;
*)
echo "
Enable profiler         : NO"
echo
;;
esac

if test -z "$PYTHON_INCLUDES"; then
echo "
Build Python bindings   : NO"
else
echo "
Build Python bindings   : YES"
fi

dnl <= CFLAGS and LDFLAGS =>
echo "
CFLAGS                  : $CFLAGS
LDFLAGS                 : $LDFLAGS"
echo
