/* $Id$
 * 
 * eazel-proxy-util-gtk: GTK-dependent portions of eazel-proxy-util
 *
 * Copyright (C) 2000 Eazel, Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author:  Michael Fleming <mfleming@eazel.com>
 *
 */

#ifndef EAZEL_PROXY_UTIL_GTK_H
#define EAZEL_PROXY_UTIL_GTK_H

#include <eazelproxy.h>

void eazel_proxy_util_do_dialog (void);
#ifdef ENABLE_REAUTHN
void eazel_proxy_util_do_reauthn_listen (EazelProxy_UserControl cntl);
#endif /* ENABLE_REAUTHN */
EazelProxy_User *eazel_proxy_util_login_dialog (EazelProxy_UserControl cntl, char *username);

gboolean
eazel_proxy_util_do_prompt_dialog (
	const char *user, 
	const char *pw, 
	gboolean retry, 
	char **p_user, 
	char **p_pw
); 
void eazel_proxy_util_do_error_dialog (void);
void eazel_proxy_util_do_network_error_dialog (void);

/* this is in eazel-proxy-util.c */
EazelProxy_User *
eazel_proxy_util_do_login (EazelProxy_UserControl cntl, const char *login, const char *passwd, const char *target_path,
	  const char *login_path, /*OUT*/ CORBA_long *p_fail_code);

#endif /* EAZEL_PROXY_UTIL_GTK_H */
