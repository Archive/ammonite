#!/usr/bin/perl -w
#
# ammonite test script
#
# 7/5/00 mfleming
#

use POSIX ":sys_wait_h";

$PROXY="../eazel-proxy";
$PORT=31000;
$PORT_ALT=31001;
$OPTIONS="-d -e -n -p $PORT";
$TEST_URL="http://www.yahoo.com/";
$TEST_SSL_HOST= "http://mfleming:8443";
$SSL_CERT="./snakeoil-ca-rsa.crt";

$| = 1;


$ENV{"http_proxy"}="http://localhost:$PORT";

#
# Test 1 -- simple connect
#

&run_proxy ("2>test1.log");

print "test 1...";
sleep(3);
system ("wget -q -O - $TEST_URL >/dev/null 2>/dev/null");

if( 0 == $? ) {
	printf "ok\n";
} else {
	printf "FAILED\n";
}

&kill_proxy;

#
# Test 2 -- forced hostname redirect
#

&run_proxy ("-t http://www.yahoo.com:80 2>test2.log");

print "test 2...";
sleep(3);
system ("wget -q -O - http://krak.krak >/dev/null 2>/dev/null");
if( 0 == $? ) {
	printf "ok\n";
} else {
	printf "FAILED\n";
}

&kill_proxy;

#
# Test 3 -- upstream proxy with forced url rewrite
#

$pid0 = &run_proxy ("-u localhost:$PORT_ALT -t http://www.yahoo.com:80/ 2>test3-1.log");
$pid1 = &run_proxy_nodefault ("-d -e -n -p $PORT_ALT 2>test3-2.log");

print "test 3...";
sleep(3);
system ("wget -q -O - http://krak.krak >/dev/null 2>/dev/null");
if( 0 == $? ) {
	printf "ok\n";
} else {
	printf "FAILED\n";
}

kill 15,$pid0,$pid1;
waitpid $pid0,0;
waitpid $pid1,0;

#
# Test 4 -- SSL connection
#

&run_proxy ("-S $SSL_CERT -t $TEST_SSL_HOST 2>test4.log");

print "test 4...";
sleep(3);
system ("wget -q -O - http://krak.krak >/dev/null 2>/dev/null");
if( 0 == $? ) {
	printf "ok\n";
} else {
	printf "FAILED\n";
}
&kill_proxy();

#
# Test 5 -- SSL connection, upstream proxy with forced url rewrite
#

$pid0 = &run_proxy ("-u localhost:$PORT_ALT -S $SSL_CERT -t $TEST_SSL_HOST 2>test5-1.log");
$pid1 = &run_proxy_nodefault ("-d -e -n -p $PORT_ALT 2>test5-2.log");

print "test 5...";
sleep(3);
system ("wget -q -O - http://krak.krak >/dev/null 2>/dev/null");
if( 0 == $? ) {
	printf "ok\n";
} else {
	printf "FAILED\n";
}

kill 15,$pid0,$pid1;
waitpid $pid0,0;
waitpid $pid1,0;

#
# Test 6 -- bad host check
# (test succeeds if wget fails)
#

&run_proxy ("2>test6.log");

print "test 6...";
sleep(3);
system ("wget -q -O - http://krak.krak >/dev/null 2>/dev/null");
if( 0 == $? ) {
	printf "FAILED\n";
} else {
	printf "ok\n";
}

&kill_proxy();

sub run_proxy_nodefault {
	my ($pid);

	$pid = fork();
	
	if ( 0 == $pid ) {
		exec "exec $PROXY ".join(" ",@_);
	} 

	$PROXY_PID = $pid;

	return $pid;
}

sub run_proxy {
	return run_proxy_nodefault ($OPTIONS." ".join(" ",@_));
}

sub kill_proxy {
	kill 15, $PROXY_PID;
	waitpid $PROXY_PID,0;
	$PROXY_PID = 0;
}
