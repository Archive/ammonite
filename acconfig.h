/*
 * These are the defaults, but they can be changed by configure at compile
 * time, or on the command line at runtime.
 */
#undef DEFAULT_LOG

/* define for openssl support (allows outgoing connections to use ssl) */
#undef HAVE_OPENSSL

#undef ENABLE_PROFILER

#undef ENABLE_NLS
#undef HAVE_CATGETS
#undef HAVE_GETTEXT
#undef HAVE_LC_MESSAGES
#undef HAVE_STPCPY
