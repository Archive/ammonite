# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: ammonite 0.8.4\n"
"POT-Creation-Date: 2001-03-17 14:09+0100\n"
"PO-Revision-Date: 2001-02-26 12:11+0100\n"
"Last-Translator: Unknown\n"
"Language-Team: Romanian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-2\n"
"Content-Transfer-Encoding: 8bit\n"

#: libammonite/ammonite-general.c:587
msgid "(invalid)"
msgstr "(invalid)"

#: libammonite/ammonite-general.c:588
msgid "Argument Invalid"
msgstr "Argument invalid"

#: libammonite/ammonite-general.c:589
msgid "User Exists"
msgstr "Utilizatorul exist�"

#: libammonite/ammonite-general.c:590
msgid "Insufficient Resources"
msgstr "Resurse insuficiente"

#: libammonite/ammonite-general.c:591
msgid "Authentication Failure"
msgstr "Autentificare e�uat�"

#: libammonite/ammonite-general.c:592
msgid "Network Error"
msgstr "Eroare de re�ea"

#: libammonite/ammonite-general.c:593
msgid "Unexpected Server Response"
msgstr "R�spuns nea�teptat de la server"

#: libammonite/ammonite-general.c:594
msgid "User Canceled"
msgstr "Utilizator abandonat"

#: libammonite/ammonite-general.c:595
msgid "User Account not activated"
msgstr "Contul utilizator nu este activat"

#: libammonite/ammonite-general.c:596
msgid "User Disabled"
msgstr "Utilizator dezactivat"

#: libammonite/ammonite-general.c:603
msgid "(unknown)"
msgstr "(necunoscut)"

#: libammonite-gtk/ammonite-login-dialog.c:67
#: libammonite-gtk/ammonite-login-dialog.c:84
msgid "I forgot my password"
msgstr "Mi-am uitat parola"

#: libammonite-gtk/ammonite-login-dialog.c:75
#: libammonite-gtk/ammonite-login-dialog.c:83
msgid "Register"
msgstr "�nregistrare"

#: libammonite-gtk/ammonite-login-dialog.c:335
#, fuzzy
msgid "User Name:"
msgstr "Utilizator abandonat"

#: libammonite-gtk/ammonite-login-dialog.c:342
msgid "Password:"
msgstr ""

#: libammonite-gtk/ammonite-login-dialog.c:365
msgid ""
"If you do not already have an Eazel Service account and would like one, "
"please click on the \"Register for Eazel Services\" button below.\n"
"\n"
"If you have an account and need help remembering your password, please click "
"the \"I forgot my password\" button below. \n"
"\n"
"Otherwise, click the \"OK\" button to cancel the current operation."
msgstr ""
"Dac� nu ave�i �nc� un cont de servicii Eazel �i dori�i unul, v� rog s� "
"face�i clic pe butonul \"�nregistrare pentru serviciile Eazel\" de mai jos.\n"
"\n"
"Dac� ave�i un cont �i ave�i nevoie de ajutor pentru reamintirea parolei, v� "
"rog sa face�i clic pe butonul \"Mi-am uitat parola\" de mai jos.\n"
"\n"
"Altfel, face�i clic pe butonul \"OK\" pentru a abandona opera�ia curent�."

#: libammonite-gtk/ammonite-login-dialog.c:630
msgid ""
"Before you can continue, you need to log in to your Eazel Service account:"
msgstr ""
"�nainte de a continua, trebuie s� face�i login �n contul de servicii Eazel:"

#: libammonite-gtk/ammonite-login-dialog.c:633
msgid "Oops!  Your user name or password were not correct.  Please try again:"
msgstr ""
"Oops! Numele de utilizator sau parola nu au fost corecte. V� rog s� "
"�ncerca�i din nou:"

#: libammonite-gtk/ammonite-login-dialog.c:636
msgid "We're sorry, but your name or password are still not recognized."
msgstr "Ne pare r�u, �ns� numele sau parola dvs. �nc� nu sunt recunoscute."

#: libammonite-gtk/libammonite-gtk.c:627
msgid ""
"I'm sorry, network problems are preventing you from connecting to Eazel "
"Services."
msgstr ""
"Imi pare r�u, �ns� probleme de re�ea �mpiedic� conectarea la serviciile Eazel"

#: libammonite-gtk/libammonite-gtk.c:629
msgid ""
"Your Eazel Services account has not yet been activated.  You can't log in to "
"Eazel Services until you activate your account.\n"
"\n"
"Please check your email messages for activation instructions."
msgstr ""
"Contul dvs. de servicii Eazel nu a fost �nc� activat. Nu v� pute�i logina la "
"serviciile Eazel p�n� nu vi se activeaz� contul.\n"
"\n"
"V� rog s� v� verifica�i mesajele de email pentru instruc�iuni de activare."

#: libammonite-gtk/libammonite-gtk.c:633
msgid ""
"Your Eazel Services has been temporarily disabled.\n"
"Please try again in a few minutes or contact Eazel support if this continues."
msgstr ""
"Serviciile dvs. Eazel au fost temporar dezactivate.\n"
"V� rog s� �ncerca�i din nou �n c�teva minute sau contacta�i suportul Eazel "
"dac� aceast� situa�ie continu�."

#: src/EazelProxy_UserControl_ammonite.oaf.in.h:1
msgid "Eazel Ammonite Authentication Proxy"
msgstr ""
