#include "Python.h"

/* $Id$
 * Copyright (C) 2001 Eazel, Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author:  Michael Fleming <mfleming@eazel.com>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <Python.h>
#include <liboaf/liboaf.h>
#include <gconf/gconf.h>
#include <orb/orbit.h>
#include <libammonite.h>
#include <glib.h>

#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>

PortableServer_POA gl_poa;

void initpyammonite (void);

static void
raise_python_exception_from_ammonite_error (AmmoniteError err)
{
	switch (err) {
	case ERR_UserNotLoggedIn:
		PyErr_SetString (PyExc_RuntimeError, "Ammonite error: UserNotLoggedIn");
		break;
	case ERR_BadURL:
		PyErr_SetString (PyExc_RuntimeError, "Ammonite error: BadURL");
		break;
	case ERR_CORBA:
		PyErr_SetString (PyExc_RuntimeError, "Ammonite error: CORBA");
		break;
	default:
	}
}

/* NOTE: frees string too */
static PyObject *
glib_string_to_python_and_free (char *string)
{
	PyObject * ret;

	if (NULL != string) {
		ret = Py_BuildValue ("s", string);
	} else {
		ret = Py_None;
	}

	g_free (string);
	
	return ret;
}

/**************************************************************************/
/**************************************************************************/

static PyObject *
get_default_user_username (PyObject *self, PyObject *args)
{
	return Py_BuildValue ("s", ammonite_get_default_user_username());
}

static PyObject *
http_url_for_eazel_url (PyObject *self, PyObject *args)
{
	char *eazel_url_py;
	char *http_url_g;
	AmmoniteError err;
	PyObject *ret;

	if (!PyArg_Parse (args, "(s)", &eazel_url_py)) {
		return NULL;
	} else {
		err = ammonite_http_url_for_eazel_url (eazel_url_py, &http_url_g);


		if (ERR_Success != err) {
			raise_python_exception_from_ammonite_error (err);
			ret = NULL;
		} else {
			ret = glib_string_to_python_and_free (g_strconcat ("http", http_url_g, NULL));
			g_free (http_url_g);
			http_url_g = NULL; 
		}
	}

	return ret;
}

static PyObject *
eazel_url_for_http_url (PyObject *self, PyObject *args)
{
	char *http_url_py;
	char *eazel_url_g;
	AmmoniteError err;
	PyObject *ret;

	if (!PyArg_Parse (args, "(s)", &http_url_py)) {
		return NULL;
	} else {
		err = ammonite_eazel_url_for_http_url (http_url_py, &eazel_url_g);

		if (ERR_Success != err) {
			raise_python_exception_from_ammonite_error (err);
			ret = NULL;
		} else {
			ret = glib_string_to_python_and_free (eazel_url_g);
			eazel_url_g = NULL; 
		}
	}

	return ret;
}

static PyObject *
get_previous_default_username (PyObject *self, PyObject *args)
{
	PyObject *ret;

	if (!PyArg_Parse (args, "()")) {
		return NULL;
	} else {
		ret = glib_string_to_python_and_free (
			ammonite_get_previous_default_username());
	}
	return ret;
}

static PyObject *
save_previous_default_username (PyObject *self, PyObject *args)
{
	char *username_py;

	if (!PyArg_Parse (args, "(s)", &username_py)) {
		return NULL;
	} else {
		ammonite_save_previous_default_username (username_py);
	}

	return Py_None;
}

static pid_t
exec_open_child (char **argv, /*OUT*/ FILE ** p_from_stream, /*OUT*/ FILE ** p_to_stream)
{
	int err;
	int child_out_fd;
	int child_in_fd;
	void *sigpipe_old;
	int pipe_to_child[2] 	= { -1, -1 };
	int pipe_to_parent[2] 	= { -1, -1 };
	pid_t child_pid;

	g_assert ( NULL != p_from_stream );
	g_assert ( NULL != p_to_stream );

	*p_to_stream = NULL;
	*p_from_stream = NULL;

	sigpipe_old = signal (SIGPIPE, SIG_IGN);

	err = pipe (pipe_to_child);

	if ( 0 != err ) {
		child_pid = -1;
		goto error;
	}

	err = pipe (pipe_to_parent);

	if ( 0 != err ) {
		child_pid = -1;
		goto error;
	}

	child_out_fd = pipe_to_parent[1];
	child_in_fd = pipe_to_child[0];

	err = fcntl ( pipe_to_parent[0], F_SETFD, FD_CLOEXEC ) ;
	g_assert (0 == err );
	err = fcntl ( pipe_to_child[1], F_SETFD, FD_CLOEXEC ) ;
	g_assert (0 == err );

	child_pid = fork ();
	if (child_pid == 0) {
		if ( -1 == dup2 (child_in_fd, STDIN_FILENO) ) {
			_exit (-1);
		}

		if ( -1 == dup2 (child_out_fd, STDOUT_FILENO) ) {
			_exit (-1);
		}

		execv (argv[0], (char **) argv);
		_exit (1);
	}

	close (pipe_to_parent[1]);
	pipe_to_parent[1] = -1;
	close (pipe_to_child[0]);
	pipe_to_child[0] = -1;
			
	if ( -1 == child_pid ) {
		g_warning ("fork returned error %d", errno);
		goto error;
	}

	*p_to_stream = fdopen (pipe_to_child[1], "w");
	g_assert ( NULL != *p_to_stream );
	pipe_to_child[1] = -1;

	*p_from_stream = fdopen (pipe_to_parent[0], "r");
	g_assert ( NULL != *p_from_stream );
	pipe_to_parent[0] = -1;

	setvbuf (*p_to_stream, NULL, _IOLBF, 0);
	setvbuf (*p_from_stream, NULL, _IOLBF, 0);

error:
	if ( -1 != pipe_to_parent[0] ) { close ( pipe_to_parent[0]); }
	if ( -1 != pipe_to_parent[1] ) { close ( pipe_to_parent[1]); }
	if ( -1 != pipe_to_child[0] ) { close ( pipe_to_child[0]); }
	if ( -1 != pipe_to_child[1] ) { close ( pipe_to_child[1]); }
	
	signal (SIGPIPE, sigpipe_old);
	
	return child_pid;
}

#define GETLINE_DELTA 256
static char *
strdup_getline (FILE* stream)
{
	char *ret;
	size_t ret_size;
	size_t ret_used;
	int char_read;
	gboolean got_newline;

	ret = g_malloc( GETLINE_DELTA * sizeof(char) );
	ret_size = GETLINE_DELTA;

	for ( ret_used = 0, got_newline = FALSE ;
	      !got_newline && (EOF != (char_read = fgetc (stream))) ; 
	      ret_used++
	) {
		if (ret_size == (ret_used + 1)) {
			ret_size += GETLINE_DELTA;
			ret = g_realloc (ret, ret_size); 
		}
		if ('\n' == char_read || '\r' == char_read ) {
			got_newline = TRUE;
			ret [ret_used] = '\0';
		} else {
			ret [ret_used] = char_read;
		}
	}

	if (got_newline) {
		return ret;
	} else {
		g_free (ret);
		return NULL;
	}
}
#undef GETLINE_DELTA


static gboolean
url_xlate_fork_exec (const char *eazel_url, /* OUT */ char **p_http_url)
{
	char *argv[] = {INSTALL_PATH_BIN "/eazel-proxy-util", "-x", NULL};
	FILE *from, *to;
	char *scheme_end;
	char *result;
	pid_t child_pid;
	int child_status;

	g_return_val_if_fail (eazel_url != NULL, FALSE);

	scheme_end = strchr (eazel_url, ':');

	if (NULL == scheme_end) {
		return FALSE;
	}

	child_pid = exec_open_child (argv, &from, &to);

	if (-1 == child_pid) {
		return FALSE;
	}

	fprintf (to, "%s\n", scheme_end+1);
	
	result = strdup_getline (from);

	if (result == NULL ||  ':' == result[ strlen(result) - 1 ]) {
		*p_http_url = NULL;
	} else {
		*p_http_url = g_strconcat ("http:", result, NULL);
	}

	g_free (result);
	result = NULL;

	kill (child_pid, SIGTERM);
	waitpid (child_pid, &child_status, 0);

	return *p_http_url == NULL ? FALSE : TRUE;
}

static PyObject *
http_url_for_eazel_url_prompt (PyObject *self, PyObject *args)
{
	char *eazel_url_py;
	char *http_url_g;
	AmmoniteError err;
	PyObject *ret;

	if (!PyArg_Parse (args, "(s)", &eazel_url_py)) {
		return NULL;
	} else {
		err = ammonite_http_url_for_eazel_url (eazel_url_py, &http_url_g);

		if (ERR_Success == err) {
			ret = glib_string_to_python_and_free (g_strconcat ("http", http_url_g, NULL));
			g_free (http_url_g);
			http_url_g = NULL; 
		} else if (ERR_UserNotLoggedIn == err) {
			/* Cause the modal login dialog to appear */
			if ( url_xlate_fork_exec (eazel_url_py, &http_url_g)) {
				ret = glib_string_to_python_and_free (http_url_g);
				http_url_g = NULL;
			} else {
				raise_python_exception_from_ammonite_error (ERR_UserNotLoggedIn);
				ret = NULL;
			}
		} else {
			raise_python_exception_from_ammonite_error (err);
			ret = NULL;
		}
	}

	return ret;	
}

static struct PyMethodDef ammonite_methods[] = {
	{"get_default_user_username", get_default_user_username, METH_VARARGS},
	{"http_url_for_eazel_url", http_url_for_eazel_url, METH_VARARGS},
	{"eazel_url_for_http_url", eazel_url_for_http_url, METH_VARARGS},
	{"get_previous_default_username", get_previous_default_username, METH_VARARGS},
	{"save_previous_default_username", save_previous_default_username, METH_VARARGS},
	{"http_url_for_eazel_url_prompt", http_url_for_eazel_url_prompt, METH_VARARGS},
	{NULL, NULL}
};


/* Python module init function */
void
initpyammonite (void)
{
	char * fake_argv[] = {"pyammonite", NULL};
	CORBA_ORB orb;
	CORBA_Environment ev;
	GError *gerror = NULL;
	PyObject *self;

	CORBA_exception_init (&ev);
	
	orb = oaf_init (1, fake_argv);

	/* Get the POA for further object activation */
	gl_poa = (PortableServer_POA) CORBA_ORB_resolve_initial_references (orb, "RootPOA", &ev);
	PortableServer_POAManager_activate (PortableServer_POA__get_the_POAManager (gl_poa, &ev), &ev);

	/* init gconf, which is needed by ammonite */
	if (!gconf_init (1, fake_argv, &gerror)) {
		fprintf (stderr, "GConf init failed:\n  %s", gerror->message);
		exit (-1);
	}

	if (!ammonite_init (gl_poa)) {
		fprintf (stderr, "Error during ammonite_init()\n");
		exit (-1);
	}

	self = Py_InitModule ("pyammonite", ammonite_methods);

	PyObject_SetAttrString (self, "__doc__", Py_BuildValue("s", 
"Python interface to Ammonite (Eazel Services Login Session Manager)\n\n"
"Functions:\n\n"
"get_default_user_username() -- \n"
"  Returns username string of currently logged in default (primary) Eazel\n"
"  Service User or None if no user is currently logged in\n\n"
"http_url_for_eazel_url(string) -- \n"
"  Translates an 'eazel-services:' URL into an 'http://localhost:<portnum>/'\n"
"  URL Raises RuntimeException if no user is logged in\n\n"
"http_url_for_eazel_url_prompt(string) --\n"
"  Translates an 'eazel-services:' URL into an http URL but prompts the user\n"
"  to log in if there is no login session.  This is probably the function\n"
"  you want to use.\n\n"
"eazel_url_for_http_url() -- \n"
"  Translates an 'http://localhost:<portnum>/' into an 'eazel-services:'\n"
"   URL.  Raises an exception on URL's that cannot be translated\n\n"
"get_previous_default_username() -- \n"
"  Retrieves the username used during the previous login session (nice for\n"
"  login dialogs\n\n"
"save_previous_default_username() -- \n"
"  Set the value returned by get_previous_default_username ()\n\n"
));


	CORBA_exception_free (&ev);

}

